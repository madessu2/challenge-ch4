function getComputerChoice() {
    const com = Math.random();
    if( com < 0.34) return 'batu';
    if( com >= 0.34 && com < 0.67) return 'kertas';
    return 'gunting';
}

function getHasil(com, player1) {
    if (player1 == com) return 'DRAW';
    if (player1 == 'batu') return (com == 'gunting') ? 'PLAYER 1 WIN' : 'COM WIN';
    if (player1 == 'kertas') return (com == 'batu') ? 'PLAYER 1 WIN' : 'COM WIN';
    if (player1 == 'gunting') return (com == 'batu') ? 'COM WIN' : 'PLAYER 1 WIN';
    } 

const batuCom = document.querySelector('.batu-com');
const guntingCom = document.querySelector('.gunting-com');
const kertasCom = document.querySelector('.kertas-com');
const info = document.querySelector('.info');


const batu = document.querySelector('.batu');
batu.addEventListener('click', function() {
    const computerChoice = getComputerChoice();
    const player1Choice = batu.className; 
    const hasil = getHasil(computerChoice, player1Choice);
    
    // const computerOut = document.getElementsByClassName('.batu-com');
    // computerOut.addEventListener('change', event => {
    //     document.body.style.backgroundColor = gray;
    // });

    const info = document.querySelector('.info');
    info.classList.add('hasil');
    info.innerHTML = hasil;
    computerChoice.style.backgroundColor = 'white';
    console.log(player1Choice)
    console.log(computerChoice)
    console.log(hasil)
    
});

const gunting = document.querySelector('.gunting');
gunting.addEventListener('click', function() {
    const computerChoice = getComputerChoice();
    const player1Choice = gunting.className; 
    const hasil = getHasil(computerChoice, player1Choice);

    const computerOut = document.querySelector('.option-com');
    computerOut.setAttribute('onclick', '.' + computerChoice + '-com');

    const info = document.querySelector('.info');
    info.classList.add('hasil');
    info.innerHTML = hasil;
    console.log(player1Choice)
    console.log(computerChoice)
    console.log(hasil)

});

const kertas = document.querySelector('.kertas');
kertas.addEventListener('click', function() {
    const computerChoice = getComputerChoice();
    const player1Choice = kertas.className; 
    const hasil = getHasil(computerChoice, player1Choice);

    const computerOut = document.querySelector('.option-com');
    computerOut.setAttribute('onclick', '.' + computerChoice + '-com');

    const info = document.querySelector('.info');
    info.innerHTML = hasil;
    info.classList.add('hasil');
    console.log(player1Choice)

    console.log(computerChoice)
    console.log(hasil)

});
